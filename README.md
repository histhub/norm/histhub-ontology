# histHub Ontology

The histHub ontology models persons, places, organizations, families, concepts,
thesauri, lemmas inside the histHub digital history project.

The ontology is described in an
[eavlib](https://gitlab.com/histhub/norm/eavlib.git)-compatible YAML file
`histhub_ontology.yml`.
